/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  ** This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * COPYRIGHT(c) 2019 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "adc.h"
#include "dma.h"
#include "tim.h"
#include "usart.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "arm_math.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
#define SampleSize 12500
#define TEST_LENGTH_SAMPLES   100
#define NUM_STAGES 			  2

static float32_t iirStateF32[2*NUM_STAGES];
//const float32_t iirCoeffs32[5*NUM_STAGES] = {
//		1.0f, 2.0f,  1.0f ,   1.349063924409788395308851249865256249905f,  -0.686994879909361588765648320986656472087f,
//		1.0f,  2.0f,  1.0f,    1.07647258167285575503058225876884534955f,   -0.346121337014787144870098245519329793751f,
//		1.0f,  1.0f,  0 ,   0.499671677371020306335225313887349329889f , 0
//};  // 200k fs 5th
//float32_t scaledV = 0.084482738874893339997562691223720321432f*0.067412188835482819704303381058707600459f*0.250164161314489874587962958685238845646;
//const float32_t iirCoeffs32[5*NUM_STAGES] = {
//		1.0f,  2.0f,  1.0f,    1.51039332657469138609940273454412817955f,   -0.735717678558940435529223123012343421578f,
//		1.0f,  2.0f,  1.0f,    1.244341955159607993053327845700550824404f,  -0.42997608089360200844808446163369808346f,
//		1.0f,  1.0f,  0.0f,    0.582948895253000975991142240673070773482f,  0
//};// 250K fs 5th
//float32_t scaledV = 0.056331087996062206846303865859226789325f*0.046408531433498524665370865704971947707f*0.208525552373499484248853264034551102668f;
const float32_t iirCoeffs32[5*NUM_STAGES] = {
		1.0f,  2.0f,  1.0f,    1.464267326462597385017261331086046993732f,  -0.682710483395187917032842506159795448184f,
		1.0f,  2.0f,  1.0f,    1.195947461580932458247161775943823158741f,  -0.374361972587185176664092978171538561583};  // 250k fs 4th
float32_t scaledV = 0.054610789233147709331728236747949267738f*0.044603627751563172665338896649700473063f;
//const float32_t iirCoeffs32[5*NUM_STAGES] = {
//		1.0f,  2.0f,  1.0f,  1.396356095856089885032247366325464099646f,  -0.604668081153033831753873528214171528816f,
//		1.0f,  1.0f,  0.0f,  0.582948895253000975991142240673070773482f,  0  };  //250k 3rd
//float32_t scaledV = 0.052077996324235938108149213121578213759f*0.208525552373499484248853264034551102668f;

//const float32_t iirCoeffs32[5*NUM_STAGES] = {
//		1.0f,  2.0f,  1.0f,  1.290680445397921705108501555514521896839f,  -0.483227466005832606565917330954107455909f
//  };   //250k 2nd
//float32_t scaledV = 0.048136755151977746181035655581581522711f;

arm_biquad_cascade_df2T_instance_f32 S;
uint16_t IIR_counter = 0;
float32_t  IIRInput[TEST_LENGTH_SAMPLES], IIROutput[TEST_LENGTH_SAMPLES];
float32_t IIROut;
uint16_t adcdata[SampleSize];
uint8_t Flag = 0;


#define NUM_TAPS              11

arm_fir_instance_f32 FirS;
static float32_t firStateF32[TEST_LENGTH_SAMPLES + NUM_TAPS - 1];

uint16_t FIR_counter = 0;
float32_t  FIRInput[TEST_LENGTH_SAMPLES], FIROutput[TEST_LENGTH_SAMPLES];
float32_t FIROut;
/* ----------------------------------------------------------------------
** FIR Coefficients buffer generated using fir1() MATLAB function.
** fir1(28, 6/24)
** ------------------------------------------------------------------- */

const float32_t firCoeffs32[NUM_TAPS] = {
//		   0.001240083738691f,0.0005551098750168f,-0.0008248866304076f,-0.003558202220725f,
//		  -0.007633903108822f, -0.01177845887098f, -0.01336266927536f,-0.009002771486706f,
//		   0.004253818500135f,  0.02756280359542f,   0.0592917903605f,  0.09488041929888f,
//		     0.1277374071909f,   0.1509638150728f,   0.1593512879214f,   0.1509638150728f,
//		     0.1277374071909f,  0.09488041929888f,   0.0592917903605f,  0.02756280359542f,
//		   0.004253818500135f,-0.009002771486706f, -0.01336266927536f, -0.01177845887098f,
//		  -0.007633903108822f,-0.003558202220725f,-0.0008248866304076f,0.0005551098750168f,
//		   0.001240083738691f
		0.00405905996579847f,
		0.0163877517888435f,
		0.0571254273465126f,
		0.124292937208946f,
	0.189660572938473f,
	0.216948501502854f,
	0.189660572938473f,
	0.124292937208946f,
	0.0571254273465126f,
	0.0163877517888435f,
	0.00405905996579847f
};

/* ------------------------------------------------------------------
 * Global variables for FIR LPF Example
 * ------------------------------------------------------------------- */


/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_USART6_UART_Init();
  MX_ADC1_Init();
  MX_TIM8_Init();
  MX_TIM3_Init();
  MX_TIM12_Init();
  MX_TIM2_Init();
  /* USER CODE BEGIN 2 */
//HAL_TIM_Base_Start(&htim3);
HAL_TIM_PWM_Start(&htim2,TIM_CHANNEL_1);
HAL_Delay(100);
HAL_ADC_Start_DMA(&hadc1,(uint32_t*)&adcdata[0],SampleSize);
HAL_TIM_Base_Start(&htim8);
arm_biquad_cascade_df2T_init_f32(&S, NUM_STAGES, (float32_t*)&iirCoeffs32[0],  (float32_t*)&iirStateF32[0]);

arm_fir_init_f32(&FirS, NUM_TAPS, (float32_t *)&firCoeffs32[0],(float32_t*)&firStateF32[0], TEST_LENGTH_SAMPLES);

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
	char c[20];
	uint8_t l;
	while (1)
  {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */

		if(Flag==1){
			volatile uint16_t max;
			  for(uint32_t i=0;i<SampleSize;i++){
//				  if(IIR_counter == TEST_LENGTH_SAMPLES){
					  HAL_GPIO_WritePin(test_GPIO_Port,test_Pin,GPIO_PIN_SET);
					  arm_biquad_cascade_df2T_f32(&S, (float32_t*)&IIRInput[0], (float32_t*)&IIROutput[0], TEST_LENGTH_SAMPLES);
					  IIR_counter = 0;
					  for(uint8_t j=0;j<50;j++){
							if(1){

							max= IIROutput[0];
							}
					  }
					  HAL_GPIO_WritePin(test_GPIO_Port,test_Pin,GPIO_PIN_RESET);
//				  }
				  IIRInput[IIR_counter] = (float32_t)adcdata[i];
				  IIROut = IIROutput[IIR_counter]*scaledV;
				  IIR_counter += 1;


//				  if(FIR_counter == TEST_LENGTH_SAMPLES){
//					  HAL_GPIO_WritePin(test_GPIO_Port,test_Pin,GPIO_PIN_SET);
//					  arm_fir_f32(&FirS, (float32_t*)&FIRInput[0], (float32_t*)&FIROutput[0], TEST_LENGTH_SAMPLES);
//					  FIR_counter = 0;
//					  for(uint8_t j=0;j<50;j++){
//							if(1){
//
//							max= FIROutput[0];
//							}
//					  }
//					  HAL_GPIO_WritePin(test_GPIO_Port,test_Pin,GPIO_PIN_RESET);
//				  }
//				  FIRInput[FIR_counter] = (float32_t)adcdata[i];
//				  FIROut = FIROutput[FIR_counter];
//				  FIR_counter += 1;

				l = sprintf(&c[0],"%d,%d\n",(uint16_t)IIROut,adcdata[i]);
//				HAL_UART_Transmit(&huart6,(uint8_t*)&c[0],l,1000);

			 }
//			Flag = 0;
		}
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInitStruct = {0};

  /**Configure the main internal regulator output voltage 
  */
  __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);
  /**Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 12;
  RCC_OscInitStruct.PLL.PLLN = 192;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 2;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /**Activate the Over-Drive mode 
  */
  if (HAL_PWREx_EnableOverDrive() != HAL_OK)
  {
    Error_Handler();
  }
  /**Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_6) != HAL_OK)
  {
    Error_Handler();
  }
  PeriphClkInitStruct.PeriphClockSelection = RCC_PERIPHCLK_USART6;
  PeriphClkInitStruct.Usart6ClockSelection = RCC_USART6CLKSOURCE_PCLK2;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
}

/* USER CODE BEGIN 4 */
void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef* hadc){
	Flag =1;
	HAL_ADC_Stop_DMA(&hadc1);

}
/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
